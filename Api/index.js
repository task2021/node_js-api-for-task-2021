const express = require('express');
const mysql = require('mysql2');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

const connection = mysql.createPool({
    user: 'root',
    password: 'root',
    database: 'api',
    host: 'localhost',
    port: 3306,
    connectTimeout: 30000,
    connectionLimit: 10,
});

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', express.static('public'));

app.get('/', (req, res) => {
    connection.query("SELECT * FROM foods", (err, result, fields) => {
        res.status(200).json(result);
    })
});

app.listen(port, () => {
    console.log('Server is started');
})